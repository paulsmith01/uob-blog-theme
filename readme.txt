=== UOB Blog ===

Wordpress Theme for use with University of Bristol Wordpress sites

Install as with a standard Wordpress theme to /wp-content/themes/


== Credits ==

* Developed by University of Bristol Communication and Marketing http://www.bris.ac.uk/public-relations/
* Developed by Paul Smith (PJSWeb) https://www.pjsweb.uk
* Based on Underscores https://underscores.me/, (C) 2012-2017 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)
