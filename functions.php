<?php
/**
 * UOB Blog functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package uobwp
 */

if ( ! function_exists( 'uobwp_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function uobwp_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on UOB Blog, use a find and replace
		 * to change 'uobwp' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'uob-blog-theme', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		//add_theme_support( 'post-thumbnails' );

		// // This theme uses wp_nav_menu() in one location.
		// register_nav_menus( array(
		// 	'menu' => esc_html__( 'Primary', 'uob-blog-theme' ),
		// ) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'uobwp_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

	}
endif;
add_action( 'after_setup_theme', 'uobwp_setup' );

function register_default_menu() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu', 'uob-blog-theme'),
     )
   );
 }
 add_action( 'init', 'register_default_menu' );


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function uobwp_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'uobwp_content_width', 640 );
}
add_action( 'after_setup_theme', 'uobwp_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function uobwp_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'uob-blog-theme' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'uob-blog-theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar(array(
        'name'          => 'Footer Sidebar 1',
        'id'            => 'footer-sidebar-1',
        'description'   => 'Footer Sidebar 1',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="widget__h1">',
        'after_title'   => '</h2>',
    ));

    register_sidebar(array(
        'name'          => 'Footer Sidebar 2',
        'id'            => 'footer-sidebar-2',
        'description'   => 'Footer Sidebar 2',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="widget__h1">',
        'after_title'   => '</h2>',
    ));

    register_sidebar(array(
        'name'          => 'Footer Sidebar 3',
        'id'            => 'footer-sidebar-3',
        'description'   => 'Footer Sidebar 3',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="widget__h1">',
        'after_title'   => '</h2>',
    ));
}
add_action( 'widgets_init', 'uobwp_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function uobwp_scripts() {
	wp_enqueue_style( 'uobwp-style', get_stylesheet_uri() );
	wp_enqueue_style( 'uobwp-style-blog', get_template_directory_uri() . '/css/uob-blog.css' );
	wp_enqueue_script( 'uobwp-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'uobwp-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'uobwp_scripts' );

function uobwp_add_google_fonts() {
 
	wp_enqueue_style( 'uobwp-google-fonts', '//fonts.googleapis.com/css?family=Open+Sans:400italic,400,700', false ); 
}
add_action( 'wp_enqueue_scripts', 'uobwp_add_google_fonts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Remove customizer settings from Customizer menu
 */
function remove_customizer_settings( $wp_customize ){

	$wp_customize->remove_section('colors');
	$wp_customize->remove_section('background_image');
	$wp_customize->remove_section('custom_css');
	$wp_customize->remove_section('custom-logo');
	$wp_customize->remove_control('site_icon');
  
}
add_action( 'customize_register', 'remove_customizer_settings', 20 );

if (!defined('DISALLOW_FILE_EDIT')) define( 'DISALLOW_FILE_EDIT', true );



// Get CSS files

function uobwp_get_css_files($dir)
{
    //Get the list of files of the directory
    $files = ['Default'=>'default.css','Pink'=>'bright-pink.css','Orange'=>'bright-orange.css','Aqua'=>'bright-aqua.css','Bright blue'=>'bright-blue.css'];

    //Create an associative array
    //with the values of the previous array
    //removing the .css for the file name in the dropdown
    function associate($a)
    {
        $temp = array();
        foreach ($a as $key=>$value) {
            $temp[$value] = $key;
            //$temp[$value] = str_replace("-"," ",$temp[$value]);
            //$temp[$value] = str_replace("_"," ",$temp[$value]);
        }
        return $temp;
    }

    //Use the associate function only in the CSS files
    return associate(array_filter($files,
        function ($val) {
            return pathinfo($val)['extension'] == 'css';
        }));
}

function uobwp_sanitize_filename( $filename ) {
	return sanitize_text_field($filename);
}

function uobwp_customizer_register($wp_customize)
{	
	// Setting up colour scheme stylesheets
    $wp_customize->add_setting('uobwp_color_scheme', array(
        'default' => 'default.css',
        'transport' => 'refresh',
        'sanitize_callback' => 'uobwp_sanitize_filename',
    ));

    $wp_customize->add_section('uobwp_color_scheme_section', array(
        'title' => __('Colour Scheme', 'uob-blog-theme'),
        'description' => __('Choose the colour scheme for your site', 'uob-blog-theme'),
        'priority' => 4
    ));
    
    $wp_customize->add_control('uobwp_site_scheme_color',
        array('label' => __('Pick Color Scheme', 'uob-blog-theme'),
            'section' => 'uobwp_color_scheme_section',
            'priority' => 4,
            'type' => 'select',
            'settings' => 'uobwp_color_scheme',
            'choices' => uobwp_get_css_files(get_template_directory() . '/css/scheme/', 'css'))
    );
	
	// Reorder menu items in the Customizer menu
	$wp_customize->get_section('title_tagline')->priority = 1;
	$wp_customize->get_section('static_front_page')->priority = 2;
	$wp_customize->get_section('header_image')->priority = 3;

	// Site Logo Upload
	$wp_customize->add_setting( 'site_icon', array (
	    'default' => get_template_directory_uri().'/assets/logo-270x270.gif',
	    'sanitize_callback' => 'uobwp_sanitize_filename',
	) );

	

}
add_action( 'customize_register', 'uobwp_customizer_register');

// Enqueue stylesheets
function uobwp_scheme() {
    //Get the setting's value
    $colorScheme = get_theme_mod( 'uobwp_color_scheme', 'default.css');
    //register the style with the color scheme stored in the $colorScheme
    wp_register_style( 'theme', get_template_directory_uri() . '/css/scheme/' . $colorScheme);
    //Append the style to the theme
    wp_enqueue_style( 'theme' );
}
add_action( 'wp_enqueue_scripts', 'uobwp_scheme' );

// Site icons
// function uobwp_site_icon() {
//     if ( ! has_site_icon() && ! is_customize_preview() ) {

// 		$meta_tags = array();
// 		$icon_32 = get_site_icon_url( 32, get_template_directory_uri().'/assets/logo-32x32.gif' );
// 		if ( empty( $icon_32 ) && is_customize_preview() ) {
// 			$icon_32 = '/favicon.gif'; // Serve default favicon URL in customizer so element can be updated for preview.
// 		}
// 		if ( $icon_32 ) {
// 			$meta_tags[] = sprintf( '<link rel="icon" href="%s" sizes="32x32" />', esc_url( $icon_32 ) );
// 		}
// 		$icon_192 = get_site_icon_url( 192, get_template_directory_uri().'/assets/logo-192x192.gif' );
// 		if ( $icon_192 ) {
// 			$meta_tags[] = sprintf( '<link rel="icon" href="%s" sizes="192x192" />', esc_url( $icon_192 ) );
// 		}
// 		$icon_180 = get_site_icon_url( 180, get_template_directory_uri().'/assets/logo-180x180.gif' );
// 		if ( $icon_180 ) {
// 			$meta_tags[] = sprintf( '<link rel="apple-touch-icon-precomposed" href="%s" />', esc_url( $icon_180 ) );
// 		}
// 		$icon_270 = get_site_icon_url( 270, get_template_directory_uri().'/assets/logo-270x270.gif' );
// 		if ( $icon_270 ) {
// 			$meta_tags[] = sprintf( '<meta name="msapplication-TileImage" content="%s" />', esc_url( $icon_270 ) );
// 		}
	 
// 		/**
// 		 * Filters the site icon meta tags, so Plugins can add their own.
// 		 *
// 		 * @since 4.3.0
// 		 *
// 		 * @param array $meta_tags Site Icon meta elements.
// 		 */
// 		$meta_tags = apply_filters( 'site_icon_meta_tags', $meta_tags );
// 		$meta_tags = array_filter( $meta_tags );
	 
// 		foreach ( $meta_tags as $meta_tag ) {
// 			echo "$meta_tag\n";
// 		}
// 	}
// }

// register_default_headers( array(
//     'default-image' => array(
//         'url'           => '%s/assets/images/test-header.png',
//         'thumbnail_url' => '%s/assets/images/test-header.png',
//         'description'   => __( 'Default Header Image', 'uobwp' )
//     ),
// ) );


// Disable Custom CSS in the frontend head
remove_action( 'wp_head', 'wp_custom_css_cb', 11 );
remove_action( 'wp_head', 'wp_custom_css_cb', 101 );

// Add class to body when no header image is set
function custom_class( $classes ) {
    if ( has_header_image() ) {
        $classes[] = 'has-header-image';
    }
    return $classes;
}
add_filter( 'body_class', 'custom_class' );
