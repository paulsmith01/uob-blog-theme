<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @package UOB_Blog
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'uob-blog-theme' ); ?></a>

	<header id="masthead" class="site-header">
		<?php if ( has_header_image() ) { ?>
			<img src="<?php echo( get_header_image() ); ?>" alt="" class="site-header-image" />
		<?php } ?>
			
		
        <div class="site-branding-wrapper">
            <div class="logo-and-branding">
                <div class="uob-logo-wrapper">
                    <img src="<?php echo get_template_directory_uri().'/assets/images/uob-logo.svg'; ?>" class="uob-logo">

                </div>
                <div class="site-branding">
                    <p class="site-title">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
                    </p>
                    <?php
					$description = get_bloginfo( 'description', 'display' );
					if ( $description || is_customize_preview() ) : ?>
						<p class="site-description"><?php echo $description; ?></p>
					<?php 
					endif; 
					?>
                </div>
            </div>
            <!-- .site-branding -->
        </div>



		<nav id="site-navigation" class="main-navigation" style="position: absolute; top: 0">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Menu', 'uob-blog-theme' ); ?></button>
			<?php
				wp_nav_menu( array(
					'theme_location' => 'header-menu',
					'menu_id'        => 'primary-menu',
					'item_spacing'   => 'discard', 
					'container_class' => 'menu-container'
				) );
			?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
